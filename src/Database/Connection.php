<?php

namespace Jeancsil\Toy3\Database;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class Connection
{
    private $connection;

    /**
     * @param $host
     * @param $port
     * @param $user
     * @param $pass
     * @param $database
     */
    public function connect($host, $port, $user, $pass, $database)
    {
        $this->connection = mysqli_connect($host, $user, $pass, $database, $port);
    }
}
