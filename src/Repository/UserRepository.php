<?php

namespace Jeancsil\Repository;

use Jeancsil\Toy3\Database\Connection;

/**
 * @author Jean Carlos <jeancsil@gmail.com>
 */
class UserRepository
{
    /**
     * @var \Jeancsil\Toy3\Database\Connection
     */
    private $connection;

    /**
     * @param \Jeancsil\Toy3\Database\Connection $connection
     */
    public function __construct(Connection $connection)
    {
        $this->connection = $connection;
    }

    /**
     * @return mixed
     */
    public function findOrderedUsers()
    {
        return $this->connection->query('SELECT name FROM user ORDER BY name ASC LIMIT 50');
    }
}
